<?php

/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylorotwell@gmail.com>
 */

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels nice to relax.
|
*/

require __DIR__.'/bootstrap/autoload.php';
/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

$app = require __DIR__.'/bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/
//define('__STATIC__',dirname(__FILE__).'\static');

if($_SERVER['REMOTE_ADDR'] == '192.168.100.16') {

//常量
define('__STATIC__','/shop/static');
define('__ROOT__','/shop/?s=');		
} else {
//常量
define('__STATIC__','/static');
define('__ROOT__','/?s=');
}

/**重写url**/
$request_url = $_SERVER['REQUEST_URI'];
//获取s=的参数
$params = explode('?s=',$request_url);
unset($_GET['s']);
//获取路由地址
if(isset($params[1])) {
$script = explode('&',$params[1]);
//路由拼接参数
$path = (count($script) == 1) ? $script[0] : $script[0].'?';
	unset($script[0]);
	array_values($script);

	foreach($script as $k=>$v) {
		if($k == 1) {
			$path.=$v;
		} else {
			$path.='&'.$v;	
		}
	}
	$_SERVER['REQUEST_URI'] = $path;
//如果没有参数则跳转到登录页/
} else {
	$_SERVER['REQUEST_URI'] = '/';		
}
/**重写结束**/

$kernel = $app->make('Illuminate\Contracts\Http\Kernel');

$response = $kernel->handle(
	$request = Illuminate\Http\Request::capture()
);

$response->send();

$kernel->terminate($request, $response);
