$(function () {
	var _PUBLIC = window.PUBLIC;
	_init();

    //点击用户名弹出菜单
    /*$('#header').on('click','.user_info',function () {
    	var _this = $(this);
    	var menu = _this.next();
    	var offset = _this.offset();

    	if(menu.is(':hidden')) {
    		_this.find('img').attr('src',_PUBLIC+'/user_up.png');
    		menu.css({'top':offset.top+_this.height()+5,'left':offset.left});
    		menu.show();
    	} else {
    		_this.find('img').attr('src',_PUBLIC+'/user_down.png');
    		menu.hide();
    	}
    })*/
	
	$('.user_div').hover(function () {
		var _this = $(this).find('.user_info');
    	var menu = _this.next();
    	var offset = _this.offset();
		_this.find('img').attr('src',_PUBLIC+'/user_up.png');
		menu.css({'top':_this.height()+15,'left':offset.left});
		menu.show();
	},function () {
		var _this = $(this).find('.user_info');
    	var menu = _this.next();
    	var offset = _this.offset();
		_this.find('img').attr('src',_PUBLIC+'/user_down.png');
    	menu.hide();	
	})

    //系统菜单下拉
    $('#sidebar').on('click','.parent',function () {

    	var _this = $(this);
    	var son = _this.nextAll('.son');
    	//子类显示状态 true 为隐藏  false为不隐藏
    	var status = son.is(':hidden');
    	if(_this.parents('#sidebar').hasClass('sidebar_show')) {
	    	if(status) {

	    		//改变图标
	    		_this.find('.direction').attr('src',_PUBLIC+'/menu/direction_up.png');
	    		//关闭非当前的子菜单
	    		$('.side_menu li .son').not(this).slideUp();
	    		son.slideDown();

	    	} else {

	    		_this.find('.direction').attr('src',_PUBLIC+'/menu/direction_right.png');
	    		son.slideUp();

	    	}
   		}

    })

    //移到主菜单上的效果
    $('.parent').hover(function () {
    	//移上去的特效
    	var _this = $(this);
    	//获取当前的地址
    	var icon_src = _this.find('.icon').attr('src');
    	//获取图片名
    	var name = icon_src.split('/')[icon_src.split('/').length-1].split('.');
    	//替换掉地址
    	var new_src = icon_src.replace(name[0],name[0].split('_')[0]+'_hover');

    	_this.css('color','#1dafa5').find('.icon').attr('src',new_src);

    },function () {
    	//移开的特效
    	var _this = $(this);
    	//获取当前的地址
    	var icon_src = _this.find('.icon').attr('src');
    	//获取图片名
    	var name = icon_src.split('/')[icon_src.split('/').length-1].split('.');
    	//替换掉地址
    	var new_src = icon_src.replace(name[0],name[0].split('_')[0]+'_normal');

    	_this.css('color','#82878b').find('.icon').attr('src',new_src);
    })

    //收入旁边的提示框显示
    $('.shop_info .today_amount').hover(function () {
		$(this).next().show();
    },function () {
		$(this).next().hide();
    })
	
	//移到子菜单的特效
    $('.son').hover(function () {
		var _this = $(this);
		_this.not('[status=1]').removeClass('active');
		_this.addClass('active');
    },function () {
    	var _this = $(this);
		_this.not('[status=1]').removeClass('active');
    })
	
	//点击子菜单的效果
	$('.son').on('click','a',function () {
		
		var parent = $(this).parent();
		//移除其他父菜单的点击状态
		$('.son').not(parent).removeAttr('status');
		//给点击的元素加上点击状态
		parent.attr('status',1);
		if(!parent.attr('status')) parent.addClass('active');
	})

    //用户顶部模块和侧边栏悬浮
	var header = $('#header');
	var sidebar = $('#sidebar');
	header.data({top:header.offset().top,left:header.offset().left})
	$(window).on('scroll',function () {
		
		var Ostop = $(window).scrollTop();
		var Oleft = $(window).scrollLeft();
		var overTop=header.offset().top;
		var width = header.width();

		//side fixed
		if(Ostop>overTop){
			header.css({position:'fixed',top:'0px',left:header.data('left')}).width(width);
			//sidebar.css({position:'fixed',top:'50px'});//.width(sidebar.width())
		} else if(Ostop<=header.data('top')) {
			header.css({position:'static'});
		}

		//如果横向滚动条有滚动,则让菜单栏的定位为static
		if(Oleft>0) {
			//if(sidebar.hasClass('sidebar_hide')) {
				//var side_left = sidebar.offset().left;
				var side_height = sidebar.height();
				//sidebar.hide();//height:side_height,
				sidebar.css({'position':'static','float':'left','height':side_height});
			//}
		} else {
			//if(sidebar.has('sidebar_hide')) {
				sidebar.removeAttr('style');
			//}
		}
	})



	//菜单按钮变色
	$('.shop_info').find('img').hover(function () {
		//移上去的特效
    	var _this = $(this);
    	_this.attr('src',_PUBLIC+'/stat_hover.png');

	},function () {
		var _this = $(this);
    	_this.attr('src',_PUBLIC+'/stat.png');
	})

	//菜单按钮
	$('.shop_info').on('click','img',function () {
		var _this = $(this);
		var sidebar = $('#sidebar');
		if(sidebar.hasClass('sidebar_hide')) {
			sidebar.removeClass('sidebar_hide').addClass('sidebar_show');
		} else {
			sidebar.removeClass('sidebar_show').addClass('sidebar_hide');
		}
		$('.son').removeAttr('style');
		_init();
	})

	//显示子菜单(鼠标移上去子菜单显示)
	 $('#sidebar').on('mouseenter','.parent',function () {
	 	var _this = $(this);
	 	var son = _this.nextAll('.son');
	 	var offset = _this.offset();
	 	var height = (_this.height()*_this.index('.parent'));//计算每个父菜单的
	 	if(_this.parents('#sidebar').hasClass('sidebar_hide')) {
		 	son.each(function (k,v) {
		 		$(v).css({'left':_this.width(),'top':(k)*_this.height()+height});
		 	})
		 	son.css({'width':$('#header .logo').width()}).show();
		}
	 })
	 
	 //隐藏子菜单(离开后子菜单消失)
	 $('#sidebar').on('mouseleave','li',function () {
	 	var _this = $(this).find('.parent');
	 	var son = _this.nextAll('.son');
	 	if(_this.parents('#sidebar').hasClass('sidebar_hide')) {
	 		son.hide();
	 	}
	 })
	 
	 //如果浏览器宽度变化,则重新计算irame的宽度
    $(window).resize(_init);
})

function _init() {
	//初始化irame高度自适应
	$('#header').width('100%');
    $('#container').width($('#header').width()-$('#sidebar').width());
}