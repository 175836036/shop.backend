// JavaScript Document
//存放一些公共的方法便于维护
$(function () {
	(function ($,w) {
			//IE兼容placeholder
			/*$('[placeholder]').focus(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
					input.val('');
					input.removeClass('placeholder');
				}
			}).blur(function() {
				var input = $(this);
				if (input.val() == '' || input.val() == input.attr('placeholder')) {
					input.addClass('placeholder');
					input.val(input.attr('placeholder'));
				}
			}).blur();*/
			
			/*$(document).on('focus','[placeholder]',function () {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
					input.val('');
					input.removeClass('placeholder');
				}	
			})
			
			$(document).on('blur','[placeholder]',function () {
				var input = $(this);
				if (input.val() == '' || input.val() == input.attr('placeholder')) {
					input.addClass('placeholder');
					input.val(input.attr('placeholder'));
				}	
			})*/
			//常用表单获取焦点后清除提示div
			$('#content').on('focus','.form_list input',function () {
				$(this).parent().siblings('.error_list').remove();	
			})
			
			//点击上传按钮提示框消失
			$('.upload_div').on('click','#change_img',function () {
				$('.upload_div').find('.error_div').hide();	
			})
			
			//点击取消,返回上一页
			/*$('#header').on('click','a.confirm',function (e) {
				var _this = $(this);
				return false;
			})*/
			
			//点击取消,返回上一页
			$('#header').on('click','.cancel',function () {
				var _this = $(this);
				if(_this.data('url') == undefined) {
					window.history.back();
				} else {
					window.location.href = _this.data('url');
				}
			})
			
			//弹窗里的取消,点击取消弹窗效果
			$('#change_list').on('click','.cancel',function () {
				var _this = $(this);
				_this.next().removeData('ids goods_info');
				$('#change_list').find('tbody').empty();
				$('#dialog').hide();
				$('#change_list').hide();
			})
			
			//导出文档
			$('#export').on('click',function () {
				var search_data = $('#search').data('search_data');
				var input = $('input[name=export_data]');
				if(search_data == undefined) search_data = '{}';
				input.val(search_data);
				input.parent().submit();
			})
			
	})($,window)
})

//创建资源和创建商品的一些公共特效
$(function () {
	
	//编辑资源属性名的js特效
	$('.attr_name').on('click','div.name',function () {
		$(this).focus();
	})
	
	//获取焦点隐藏div显示textarea
	$('.attr_name').on('focus','div.name',function () {
		var _this = $(this);
		var text = _this.text();
		$('.attr_list .error_list').hide();
		if(!_this.is(':hidden')) {
			if(text==_this.attr('placeholder')) text = '';	
			_this.hide().next().val(text).show().focus().css("display","block");
		}
		_this.next().focus();
	})
	
	//失去焦点保存数据并隐藏textarea
	$('.attr_name').on('blur','textarea.name',function () {
		var _this = $(this);
		var text = $.trim(_this.val());
		var li = $('.resource_button li.active');
		
		if($.trim(text) == '') {text = _this.prev().attr('placeholder');li.removeData('name').removeAttr('data-name');}
		if(!_this.is(':hidden')) {
			if(text !== _this.prev().attr('placeholder')) li.data('name',text);
			_this.hide().prev().text(text).show().css("display","block");	
		}
		
		_this.prev().blur();
	})
	
	//添加属性
	$('.attr_list').on('click','#add_attr',function () {
		var size = $('.resource_button li').size();
		if(size == 5) {
			alert('只能添加5个属性!');
			return ;	
		}
		if(size == 0) {
			$('.resource_button').append('<li class="first active">'+(size+1)+'</li>');
			$('.attr_name').show().next().show();
		} else {
			$('.resource_button').append('<li>'+(size+1)+'</li>');	
		}
	})
	
	//删除属性
	$('.attr_list').on('click','#remove_attr',function () {
		var size = $('.resource_button li').size();
		var active = $('.resource_button li.active');

			if(active.prev().index() == -1) {
				var li = active.next();
			} else {
				var li = active.prev();	
			}
			
			if(size == 1) {
				alert('必须保留一个属性!');
				return ;	
			}
		if(confirm('确定要删除这个扩展属性吗?')) {
			//删除选中的标签
			active.remove();
			//改变标签里的数字
			$('.resource_button li').each(function (k,v) {
				var _this = $(v);
				//如果第一个标签没有去除掉边距,则去除下
				if(k == 0 && !_this.hasClass('first')) {
					_this.addClass('first');	
				}
				_this.text(k+1);	
			})
			li.click();
		}
	})	
});

//元素淡入淡出提示错误信息
function isEmptyObject(obj) {
	return JSON.stringify(obj) == '{}';
}

function time() {
	var time = new Date().getTime()/1000;
	return time;
}

function number_format(num) {
	var int = parseInt(num);
	var float = parseFloat(num);
	if(int == float) {
		return int;
	} else {
		return float;
	}
}
//表单提示错误
function form_error(obj,info) {
	var div = obj.parents('.form_list');
	var next = div.next();
	
	if(next.hasClass('error_list')) {
		next.show().find('.error_div').text(info);
	} else {
		div.after('<div class="error_list"><div class="error_div">'+info+'</div></div>').next().show();
	}
}

//截取字符串
function strSub(str,len) {
	
	if(str.length > len) {
		return str.substr(0,len)+'...'	
	}
	
	return str;	
}

//plupload上传插件(资源封面和商品封面)
function plup_load() {
	var html = $('.upload_div .img').html();
	var uploader = new plupload.Uploader({
            runtimes : 'gears,html5,flash,silverlight,browserplus',
            browse_button : 'change_img',
            url : uploader_config.url,
            flash_swf_url:uploader_config.flash_swf_url,
            multipart:false,
            filters: {
            mime_types : [ //只允许上传图片
                { title : "images files", extensions : uploader_config.extensions }, 
              ],
              max_file_size : '10mb', //最大只能上传10mb的文件
              prevent_duplicates : true //不允许选取重复文件
            }
        });

        //添加完文件时候触发
        uploader.bind('FilesAdded',function (up,files) { 
           uploader.start();
		   $('.upload_div .img').text('上传中...');
        });

        uploader.bind('Error',function (up,error) {
            
        })

        //上传过程中不断触发
        uploader.bind('UploadProgress',function (up,files) {
            console.log('正在上传!');
        });
        
        //某个文件上传成功时候触发
        uploader.bind('FileUploaded',function (up,file,result) {
			
			var res = eval('('+result.response+')');
			
			if(res.status) {
				console.log('上传成功!返回信息为',res);
				$('#change_img').data('coverurl',res.url);
				$('.upload_div .img').html('<a href="'+res.url+'" target="_blank"><img src="'+res.url+'"/></a>');
			} else {
				alert('上传失败!');
				$('.upload_div .img').html(html);	
			}
        });
		
        uploader.init();
}

//导入邮寄信息
function import_mail(id) {
	var uploader = new plupload.Uploader({
            runtimes : 'gears,html5,flash,silverlight,browserplus',
            browse_button : id,
            url : uploader_config.url,
            flash_swf_url:uploader_config.flash_swf_url,
            multipart:false,
            filters: {
            mime_types : [ //只允许上xlsx
                { title : "xlsx files", extensions : uploader_config.extensions }, 
              ],
              /*max_file_size : '10mb', //最大只能上传10mb的文件*/
              prevent_duplicates : true //不允许选取重复文件
            }
        });

        //添加完文件时候触发
        uploader.bind('FilesAdded',function (up,files) { 
           uploader.start();
        });

        uploader.bind('Error',function (up,error) {
            console.log(error);
        })

        //上传过程中不断触发
        uploader.bind('UploadProgress',function (up,files) {
            console.log('正在上传!');
        });
        
        //某个文件上传成功时候触发
        uploader.bind('FileUploaded',function (up,file,result) {
			
			var res = eval('('+result.response+')');
			if(res.status) {
				window.location.reload();
			}
        });
		
        uploader.init();	
}