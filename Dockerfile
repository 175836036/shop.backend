FROM luozijun/uwsgi-php:latest

RUN php5enmod mcrypt
RUN mkdir /code
ADD . /code
WORKDIR /code
CMD uwsgi uwsgi.ini

EXPOSE 80
