<?php 
/*
comment:后台管理员模型
date:2015-6-30
author:hanyu
*/
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class User extends Model {
	
	protected $table = 'user_info';
	
	public $timestamps = false;
	
	//根据用户ID查询前台用户信息
	public function get_user_info($username) {
		$res = $this->where('username',$username)
					->where('status',1)
					->select('uid','username')
					->first();
					
		if(empty($res)) {
			return false;	
		} else {
			return $res->toArray();	
		}	
	}

}
