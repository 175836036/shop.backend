<?php 
/*
comment:商品模型
date:2015-7-6
author:hanyu
*/
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Goods extends Model {
	protected $table = 'goods';
	
	public $timestamps = false;
	
	//添加商品
	public function create_goods($data,$uid) {
		
		$_data = array(
			'name'=>trim($data['goods_name']),
			'nav'=>(int)$data['nav'],
			'cover_url'=>trim($data['cover_url']),
			'introduce'=>trim($data['introduce']),
			'price'=>(is_numeric($data['price'])) ? $data['price'] : 0,
			'hours'=>(int)$data['hours'],
			'quota_num'=>(int)$data['quota_num'],
			'base_number'=>(int)$data['base_number'],
			'mail'=>(int)$data['mail'],
			'stick'=>(int)$data['stick'],
			'creatime'=>time(),
			'startime'=>strtotime($data['startime']),
			'endtime'=>strtotime($data['endtime']),
			'attrs_list'=>(!isset($data['attrs_list'])) ? '' : base64_encode(gzcompress(json_encode($data['attrs_list']),9)),
			'creator'=>$uid,
			'status'=>1,
		);
		
		if($id = $this->insertGetId($_data)) {
			return $id;
		} else {
			return false;
		}
	}
	
	//编辑商品
	public function edit_goods($data,$uid,$id) {
		$_data = array(
			'name'=>trim($data['goods_name']),
			'nav'=>(int)$data['nav'],
			'cover_url'=>trim($data['cover_url']),
			'introduce'=>trim($data['introduce']),
			'price'=>(is_numeric($data['price'])) ? $data['price'] : 0,
			'hours'=>(int)$data['hours'],
			'quota_num'=>(int)$data['quota_num'],
			'base_number'=>(int)$data['base_number'],
			'mail'=>(int)$data['mail'],
			'stick'=>(int)$data['stick'],
			'startime'=>strtotime($data['startime']),
			'endtime'=>strtotime($data['endtime']),
			'attrs_list'=>(!isset($data['attrs_list'])) ? '' : base64_encode(gzcompress(json_encode($data['attrs_list']),9)),
			//'creator'=>$uid,
			'status'=>1,
		);
		
		$res = $this->where('goods_id','=',$id)
					->update($_data);
		return $res;
	}
	
	//获取商品信息
	public function goods_info($id) {
		$where = array(
			'tombstone'=>0
		);
		$res = $this->where($where)
					->where('goods_id','=',$id)
					->first();
		if(!empty($res)) {
			$res = $res->toArray();	
			@$res['attrs_list'] = json_decode(gzuncompress(base64_decode($res['attrs_list'])),true);
			return $res;
		} else {
			return false;	
		}
	}
	
	//获取商品信息
	public function goods_infos($ids) {
		$where = array(
			'tombstone'=>0
		);
		$ids_ordered = implode(',', $ids);
		$res = $this->select('goods_id','name','hours','attrs_list','goods_num','quota_num','price','mail')
					->where($where)
					->whereIn('goods_id',$ids)
					->orderByRaw(DB::raw("FIELD(goods_id, $ids_ordered)"))//按ids的顺序查询
					->get();
					
		if(!empty($res)) {
			$res = $res->toArray();	
			foreach($res as &$v) {
				@$v['attrs_list'] = json_decode(gzuncompress(base64_decode($v['attrs_list'])),true);
			}
			return $res;
		} else {
			return false;	
		}
	}
	
	//获取搜索商品列表
	public function search_goods_list($start,$data) {
		$where = array(
			'tombstone'=> 0
		);
		
		$res = $this->select('goods_id','name','startime','endtime','creatime','online','puawaytime','price','status')
					->Where(function ($query) use ($data){
						if(isset($data['goods_id']) && !empty($data['goods_id'])) {
							$query->where('goods_id','=',$data['goods_id']);	
						}
					})
					->Where(function ($query) use ($data){
						if(isset($data['name']) && !empty($data['name'])) {
							$query->where('name','like','%'.$data['name'].'%');	
						}
					})
					->where($where)
					->orderBy('goods_id','DESC')
					->skip($start)
					->take(config('params.pagesize'))
					->get();
		
		if($res) {
			return $res->toArray();	
		} else {
			return '';	
		}
	}
	
	//获取搜索商品总数
	public function search_goods_count($data) {
		$where = array(
			'tombstone'=> 0
		);
		
		$count = $this->Where(function ($query) use ($data){
						if(isset($data['goods_id']) && !empty($data['goods_id'])) {
							$query->where('goods_id','=',$data['goods_id']);	
						}
					})
					->Where(function ($query) use ($data){
						if(isset($data['name']) && !empty($data['name'])) {
							$query->where('name','like','%'.$data['name'].'%');	
						}
					})
					->where($where)
					->count();
		
		return $count;
	}
	
	//排除掉id集合后的资源列表
	public function set_goods_list($start,$data) {
		$where = array(
			'tombstone'=>0,
			'status'=>1,
		);
		if(!isset($data['ids'])) $data['ids'] = '';
		$res = $this->select('price','goods_id','name','startime','endtime','mail')
					->Where(function ($query) use ($data){
						if(isset($data['goods_id']) && !empty($data['goods_id'])) {
							$query->where('goods_id','=',$data['goods_id']);	
						}
					})
					->Where(function ($query) use ($data){
						if(isset($data['name']) && !empty($data['name'])) {
							$query->where('name','like','%'.$data['name'].'%');	
						}
					})
					->Where(function ($query) use ($data){
						if(isset($data['ids']) && !empty($data['ids'])) {
							$query->whereNotIn('goods_id', $data['ids']);
						}
					})
					->where($where)
					->orderBy('goods_id','DESC')
					->skip($start)
					->take(config('params.pagesize'))
					->get();
					
		if($res) {
			return $res->toArray();
		} else {
			return false;	
		}	
	}
	
	//排除掉id集合后资源总数
	public function set_goods_count($data) {
		$where = array(
			'tombstone'=>0,
			'status'=>1,
		);
		if(!isset($data['ids'])) $data['ids'] = '';
		$res = $this->Where(function ($query) use ($data){
						if(isset($data['goods_id']) && !empty($data['goods_id'])) {
							$query->where('goods_id','=',$data['goods_id']);	
						}
					})
					->Where(function ($query) use ($data){
						if(isset($data['name']) && !empty($data['name'])) {
							$query->where('name','like','%'.$data['name'].'%');	
						}
					})
					->Where(function ($query) use ($data){
						if(isset($data['ids']) && !empty($data['ids'])) {
							$query->whereNotIn('goods_id', $data['ids']);
						}
					})
					->where($where)
					->count();
		
		return $res;	
	}
	
	//删除商品
	public function delete_goods($id) {
		$res = $this->where('goods_id','=',$id)
					->update(array('tombstone'=>1));
		return $res;	
	}
	
	//修改商品的上下线状态
	public function line_goods($data,$id) {
		$res = $this->where('goods_id','=',$id)
					->update($data);
		return $res;	
	}

	//审核商品
	public function check_goods($id) {
		$res = $this->where('goods_id','=',$id)
					->update(array('status'=>1));
		return $res;		
	}
	

	//取消审核商品
	public function repeal_goods($id) {
		$res = $this->where('goods_id','=',$id)
					->update(array('status'=>0));
		return $res;
	}
}