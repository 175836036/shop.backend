<?php 
/*
comment:后台管理员模型
date:2015-6-30
author:hanyu
*/
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Manage extends Model {
	
	protected $table = 'admin';
	
	public $timestamps = false;
	
	//检查用户名是否重复(后台管理员)
    public function get_manager_info($username) {
		$res = $this->where('username',$username)
					->where('status',1)
					->select('aid','username','password')
					->first();
					
		if(empty($res)) {
			return false;	
		} else {
			return $res->toArray();	
		}
	}
	
	//根据用户ID查询用户信息(后台管理员)
	public function get_admin_info($id) {
		$res = $this->where('aid',$id)
					->where('status',1)
					->select('aid','username','password')
					->first();
					
		if(empty($res)) {
			return false;	
		} else {
			return $res->toArray();	
		}	
	}
	
	//修改用户密码(后台管理员)
	public function update_password($id,$password) {
		
		$res = $this->where('aid',$id)
					->update(['password'=>$password]);
		
		return $res;
	}

}
