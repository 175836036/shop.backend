<?php namespace App\Http\Middleware;
use App\Http\Controllers\CommonController;
use Closure;

class UserMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		//前置操作(检查是否登录)
		//如果在用户页面查询到已登录,则跳转到后台首页
		$common = new CommonController();
		if(!$common->get_user_info()) {
			return redirect('?s=/user/login');
		}
		return $next($request);
	}

}
