<?php namespace App\Http\Middleware;
use App\Http\Controllers\CommonController;
use App\Libs\Helper;
use Closure;

class RoleMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
    {
        $response = $next($request);
		
		/*$space = \Route::currentRouteAction();
		$space = explode('\\',$space);
		var_dump($space);*/
		//echo \URL::action('GoodsController@_goodsList');
		//$url = action('GoodsController@_goodsList');
		//var_dump(explode('index.php',$url));
		//echo route('show_verify');
        return $response;
    }

}
