<?php 
namespace App\Http\Controllers;
use App\Models\Manage;
use App\Models\Order;
use Config,Session,Request,Response,Redirect,Cookie;
use App\Libs\Helper;

class IndexController extends Controller {
	private $user_info;

	//初始化获取用户信息
	public function __construct() {
		$common = new CommonController();
		$this->user_info = $common->get_user_info();
	}
	
	//后台首页
	public function index() {
		$common = new CommonController();
		$order = new Order();
		
		//查询用户信息
		$data = $this->user_info;
		
		//计算历史销量
		$data['total_amount'] = $order->total_amount();
		
		//计算今日销量
		$data['today_amount'] = $order->today_amount();
		return view('index/index')->with($data);
	}
	
	//欢迎页面
	public function welcome() {
		return view('index/welcome')->with($this->user_info);
	}
	
	//登出
	public function logout() {
		return Redirect::to('?s=/user/login')->withCookie(Cookie::forget('user_info'));
	}
	
	//修改密码
	public function updatePwd() {
		//判断数据
		$data = Request::all();

		//如果是ajax则修改密码,否则加载视图
		if(!empty($data)) {
		
			$user_id = $this->user_info['uid'];
			
			//获取用户信息
			$manager = new Manage();
			$manager_info = $manager->get_admin_info($user_id);
			
			//验证旧密码是否正确
			if(Helper::createMd5Pwd($data['oldpwd']) !== $manager_info['password']) {
				return Response::json(array('status'=>0,'info'=>'旧密码错误!','name'=>'oldpwd'));	
			}
			
			//修改用户密码
			if($manager->update_password($user_id,Helper::createMd5Pwd($data['newpwd']))) {
				return Response::json(array('status'=>1,'info'=>'修改密码成功!','url'=>URL('admin/welcome')));	
			} else {
				return Response::json(array('status'=>0,'info'=>'修改密码失败!'));	
			}
			
		} else {
			return view('index/update_pwd')->with($this->user_info);
		}
	}
}