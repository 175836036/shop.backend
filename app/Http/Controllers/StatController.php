<?php 
/*
*comment:统计控制器
*date:2015-7-20
*author:hanyu
*/
namespace App\Http\Controllers;
use Config,Session,Request,Response,Validator,Cookie,DB;
use App\Models\Order;
use App\Models\Company;
use App\Models\Snap;
use App\Libs\Helper;

class StatController extends Controller {
	
	//邮寄订单列表
	public function mailList() {
		//$company = new Company();
		//$data = $company->Company_list();
		return view('stat.mailist');
	}
	
	//请求邮寄订单列表
	public function _mailList() {
			//接受数据
			$data = Request::all();
			$page = (int)$data['page'];
			$pageSize = (int)config('params.pagesize');
			$start = ($page-1)*$pageSize;
			
			//如果是搜索订单号,则其他条件不起作用
			if(isset($data['order_number']) && !empty($data['order_number'])) {
				$where[]= "binary order_number LIKE '%{$data['order_number']}%'";
			} else {
				//下单时间
				if(isset($data['order_time'])) {
					$startime = empty($data['order_time']['startime']) ? time() : strtotime($data['order_time']['startime']);
					$endtime = empty($data['order_time']['endtime']) ? time() : strtotime($data['order_time']['endtime']);
					$where[]= "(orders_time >= {$startime} AND orders_time <= {$endtime})";
				}
				
				//是否邮寄
				if(isset($data['mail']) && $data['mail'] !== '') $where[]= "mail_status = {$data['mail']}";
				
				//支付方式
				if(isset($data['telephone']) && !empty($data['telephone'])) $where[]= "telephone = {$data['telephone']}";
			}
			
			//过滤掉删除的表单
			$where[] = 'f.order_id IS NOT NULL';
			$where = 'WHERE '.implode(' AND ',$where);

			//查询出列表
			$list_sql = "SELECT m.order_id,m.telephone,m.mail_status,f.order_number,f.is_system,f.is_give,f.amount,f.orders_time,f.username FROM v_order_mail_info as m LEFT JOIN v_order_finally_info as f ON m.order_id = f.order_id $where ORDER BY m.order_id DESC LIMIT {$start},{$pageSize}";
			
			//查询出总数
			$count_sql = "SELECT COUNT(*) as count FROM v_order_mail_info as m LEFT JOIN v_order_finally_info as f ON m.order_id = f.order_id $where";
			
			$return['list'] = DB::select($list_sql);
			$return['count'] = DB::select($count_sql)[0]->count;
			$return['pageSize'] = $pageSize;
			$return['page'] = $page;
			if(!empty($return['list'])) {
				//转换时间
				foreach($return['list'] as $k=>$v) {
					$return['list'][$k]->orders_time = date('Y-m-d H:i',$v->orders_time);		
				}
			}
			return Response::json(array('status'=>1,'info'=>'获取成功!','data'=>$return));
	}

	//导出邮寄列表
	public function exportMailExcel() {
		set_time_limit(240);
		$data = json_decode(urldecode(Request::input('export_data')),true);

		//如果是搜索订单号,则其他条件不起作用
		if(isset($data['order_number']) && !empty($data['order_number'])) {
			$where[]= "binary order_number LIKE '%{$data['order_number']}%'";
		} else {
			//下单时间
			if(isset($data['order_time'])) {
				$startime = empty($data['order_time']['startime']) ? time() : strtotime($data['order_time']['startime']);
				$endtime = empty($data['order_time']['endtime']) ? time() : strtotime($data['order_time']['endtime']);
				$where[]= "(orders_time >= {$startime} AND orders_time <= {$endtime})";
			}
			
			//是否邮寄
			if(isset($data['mail']) && $data['mail'] !== '') $where[]= "mail_status = {$data['mail']}";
			
			//支付方式
			if(isset($data['telephone']) && !empty($data['telephone'])) $where[]= "telephone = {$data['telephone']}";
		}

		//过滤掉删除的表单
		$where[] = 'f.order_id IS NOT NULL';
		$where = 'WHERE '.implode(' AND ',$where);

		$list_sql = "SELECT m.order_id,m.receiver,m.telephone,m.address,m.mail_status,m.company_num,m.company_id,f.order_number,f.pattern,f.form,f.amount,f.orders_time,f.snap_ids,f.username FROM v_order_mail_info as m LEFT JOIN v_order_finally_info as f ON m.order_id = f.order_id $where ORDER BY m.order_id DESC";
		$map_ids = $snap_ids = $orders = array();
			
		$res = DB::select($list_sql);

		$pattern = array(
			1 => '支付宝',
			2 => '微信',
			3 => '免支付',
		);
		$form = array(
			1 => 'web',
			2 => 'app',
			3 => 'wap',
			4 => '其他',
		);
		$snap = new Snap();
		$company = new Company();
		$company_list = $company->Company_list();

		//转换时间
		foreach($res as $k=>$v) {

			$ids = array();
			$v->orders_time = date('Y-m-d H:i',$v->orders_time);	
			$v->form = $form[$v->form];
			$v->pattern = $pattern[$v->pattern];

			//如果是已邮寄状态,显示物流公司和状态信息
			if(!empty($v->mail_status)) {
				$v->company = $company_list[$v->company_id];
				$v->mail_status = '已邮寄';
			} else {
				$v->company = '';
				$v->mail_status = '未邮寄';
			}

			//获取快照id
			$ids = json_decode($v->snap_ids,true);
			foreach($ids as $v1) {
				//建立快照ID对订单的映射
				$map_ids[$v1] = $v->order_id;
			}
			
			$orders[$v->order_id] = (array)$v;
			unset($orders[$v->order_id]['order_id'],$orders[$v->order_id]['company_id']);
		}

		$snap_ids = array_keys($map_ids);
		$snap_infos = $snap->snaps_infos($snap_ids);
		
		$data = array();
		foreach($snap_infos as $v) {
			$order_id = $map_ids[$v['snap_id']];
			$v['snap_info']	= json_decode(gzuncompress(base64_decode($v['snap_info'])),true);
			$orders[$order_id]['goods_name'] = $v['snap_info']['name'];
			$data[] = $orders[$order_id];
		}

		$header = array(
					'order_number'=>'订单编号',
					'orders_time'=>'下单时间',
					'username'=>'用户名',
					'goods_name'=>'商品名称',
					'company'=>'物流公司',
					'company_num'=>'物流单号',
					'receiver'=>'收货人姓名',
					'address'=>'收货地址',
					'telephone'=>'收件人电话',
					'mail_status'=>'邮寄状态',
					'pattern'=>'支付方式',
					'form'=>'购买来源',
					'amount'=>'金额',
		);
		Helper::save_csv($data,'邮寄订单信息.csv',$header,true);
	}
	
	//导入邮寄信息
	public function importMailExcel() {
		//先在本地生成hash名字的图片,然后再curl上传到104服务器,最后再删除本地图片
		$common = new CommonController();
		$local = $common->plup_load();
		
		//如果上传失败,则直接返回信息
		if(empty($local['status'])) return Response::json(array('status'=>0,'info'=>'上传失败!'));
		
		require './app/Libs/PHPExcel.php';
		$PHPExcel = new \PHPExcel(); 
		$res = array();
		//指定读取器
		$PHPReader = new \PHPExcel_Reader_Excel2007();
		$PHPExcel = $PHPReader->load($local['filepath']);
		$sheet = $PHPExcel->getSheet(0);
		$highestColumm = $sheet->getHighestColumn(); //取得总列数
		$highestRow = $sheet->getHighestRow(); //取得总行数
		$highestColumm= \PHPExcel_Cell::columnIndexFromString($highestColumm);
		
		/**从第二行开始输出，因为excel表中第一行为列名*/ 
		for ($row = 2; $row <= $highestRow; $row++){//行数是以第1行开始
			$order_num = '';
			for ($column = 0; $column < $highestColumm; $column++) {//列数是以第0列开始
				$value = $sheet->getCellByColumnAndRow($column, $row)->getValue();
				
				if(!empty($value)) {
					if($column == 1) {
						$order_num = $value;
						//$res[$order_num]['order_num'] = $order_num;
					} else if($column == 2) {
						$res[$order_num]['company_id'] = $value;	
					} else if($column == 4) {
						$res[$order_num]['company_num'] = $value;	
					}
				}
			}
		}
		
		//查看这些订单有哪些是已邮寄的,是的话过滤掉
		$order_nums = array_keys($res);
		$order = new Order();
		$numbers = $order->filter_mails($order_nums);
		
		foreach($numbers as $k=>$v) {
			if(isset($res[$v])) {
				$res[$v]['order_id'] = $k;
				$order->update_mail_info($res[$v]);
				unset($res[$v]);
			}
		}
		
		return Response::json(array('status'=>1,'info'=>'操作成功!'));
	}
	
	//邮寄订单详情
	public function maildetail($order_id) {
		$id = (int)$order_id;
		
		$order = new Order();
		$snap = new Snap();
		$company = new Company();
		
		$data = array();
		$mail_info = $order->one_mail_info($order_id);
		
		//查询快照
		$snap_ids = json_decode($mail_info['snap_ids'],true);
		$snap_data = $snap->snaps_infos($snap_ids);

		$snap_info = array();
		
		//解密快照
		foreach($snap_data as $k=>$v) {
			$snap_info[$v['snap_id']] = json_decode(gzuncompress(base64_decode($v['snap_info'])),true);
		}

		$data = array(
			'mail_info'=>$mail_info,
			'snap_info'=>$snap_info,
			'company_list'=>$company->Company_list(),
		);
		
		return view('stat.maildetail')->with($data);
	}

	//删除邮寄信息
	public function deleteMail() {

		$ids = Request::input('ids');
		$order = new Order();

		//重置邮寄信息
		if($order->reset_mail_info($ids)) {
			return Response::json(array('status'=>1,'info'=>'操作成功!'));
		} else {
			return Response::json(array('status'=>0,'info'=>'操作失败!'));
		}
	}

	//修改邮寄信息
	public function updateMail() {

		$data = Request::input('data');
		$order = new Order();

		//更新邮寄信息
		foreach($data as $v) {
			if(!$order->update_mail_info($v)) {
				return Response::json(array('status'=>0,'info'=>'操作失败!'));
			}
		}
		
		return Response::json(array('status'=>1,'info'=>'操作成功!'));
	}
}