<?php 
namespace App\Http\Controllers;
use App\Models\Goods;
use App\Models\GoodsRelation;
use App\Models\Resource;
use App\Models\Nav;
use Config,Session,Request,Response,Validator,Cookie;
use App\Libs\Helper;

class GoodsController extends Controller {
	
	//初始化获取用户信息
	public function __construct() {
		$common = new CommonController();
		$this->user_info = $common->get_user_info();
	}
	
	//ajax获取商品列表
	public function _goodsList() {
			//接受数据
			$data = Request::all();
			
			//获取页码,每页长度,和sql查询起始索引
			$page = (int)$data['page'];unset($data['page']);
			$pageSize = (int)config('params.pagesize');
			$start = ($page-1)*$pageSize;
			
			$goods = new Goods();
			$return = array();
			
			$return['list'] = $goods->search_goods_list($start,$data);//获取查询出的商品总数
			$return['count'] = $goods->search_goods_count($data);//获取搜索出的商品总数
			$return['pageSize'] = config('params.pagesize');//返回每页长度
			$return['page'] = $page;//返回页码,因为异步关系最好和前台比对再渲染
			
			//转换时间
			foreach($return['list'] as $k=>$v) {
				$return['list'][$k]['startime'] = date('Y-m-d H:i',$v['startime']);	
				$return['list'][$k]['endtime'] = date('Y-m-d H:i',$v['endtime']);	
				$return['list'][$k]['creatime'] = date('Y-m-d H:i',$v['creatime']);	
			}
			
			return Response::json(array('status'=>1,'info'=>'获取成功','data'=>$return));
	}
	
	//创建商品
	public function createGoods() {
			$nav = new Nav();
			
			//传递数据到模板
			$data = array(
				'img_url'=>Cookie::get('goods_img'),
				'nav_info'=>$nav->nav_list(),
			);
			return view('goods.create')->with($data);
	}

	//创建商品ajax
	public function _createGoods() {
			$data = Request::all();
			
			//添加商品信息
			$resource = new Goods();
			$id = $resource->create_goods($data,$this->user_info['uid']);
			
			if(!empty($id)) {
				return Response::json(array('status'=>1,'info'=>'创建商品成功!','goods_id'=>$id));	
			} else {
				return Response::json(array('status'=>0,'info'=>'创建商品失败!'));	
			}
	}
	
	//编辑商品
	public function editGoods($goods_id) {
			$id = (int)$goods_id;
			$goods = new Goods();
			$nav = new Nav();
			
			return view('goods.edit')->with(array('goods_info'=>$goods->goods_info($id),'nav_info'=>$nav->nav_list()));
	}
	
	//编辑页面(修改数据)
	public function _editGoods() {

			$data = Request::all();
			
			$goods = new Goods();
			$id = (int)$data['goods_id'];unset($data['goods_id']);
			
			//修改数据库里的商品数据
			if($goods->edit_goods($data,$this->user_info['uid'],$id)) {
				return Response::json(array('status'=>1,'info'=>'修改商品成功!'));
			} else {
				return Response::json(array('status'=>0,'info'=>'修改商品失败!'));
			}
			
	}
	
	//已选择资源列表
	public function changeResource($goods_id) {
		$id = (int)$goods_id;
		
		$relation = new GoodsRelation();
		$ids = $relation->get_ids($id);
		
		$data = array('goods_id'=>$goods_id);
		
		//如果有关联的资源,则查询资源信息
		if(!empty($ids)) {
			$resource = new Resource();
			
			//根据资源ID集合查询资源信息
			$data['resources'] = $resource->get_resources($ids);
			
			//查询资源类型
			$data['types'] = $resource->map_types();
		}
	
		return view('goods.change')->with($data);
	}
	
	//商品预览
	public function previewGoods($goods_id) {
		$id = (int)$goods_id;
		
		//获取商品信息
		$goods = new Goods();
		$goods_info = $goods->goods_info($id);
		
		if(!empty($goods_info)) {
			//查询所属商品的资源ID	
			$relation = new GoodsRelation();
			$resource_ids = $relation->get_ids($id);
			
			//查询资源id集合对应的资源信息
			$resource = new Resource();
			$resource_info = $resource->get_resources($resource_ids);
			
			//查询资源类型
			$resource_types = $resource->map_types();
			
			return view('goods.preview')->with(array('goods_info'=>$goods_info,'resource_info'=>$resource_info,'types'=>$resource_types));
		}
	}
	
	//商品详情
	public function goodsdetail($goods_id) {
		$id = (int)$goods_id;
		
		//获取商品信息
		$goods = new Goods();
		$goods_info = $goods->goods_info($id);
		
		if(!empty($goods_info)) {
			
			$relation = new GoodsRelation();
			$resource_ids = $relation->get_ids($id);//查询所属商品的资源ID	
			$resource = new Resource();
			$resource_info = $resource->get_resources($resource_ids);//查询资源id集合对应的资源信息
			$resource_types = $resource->map_types();//查询资源类型
			return view('goods.detail')->with(array('goods_info'=>$goods_info,'resource_info'=>$resource_info,'types'=>$resource_types));
		}
	}
	
	//弹窗里的资源列表
	public function miniResourceList() {
			//接受数据
			$data = Request::all();
			
			$page = (int)$data['page'];unset($data['page']);//接受页码
			$pageSize = (int)config('params.pagesize');//接受每页显示长度
			$start = ($page-1)*$pageSize;//接受sql查询起始索引
			
			$resource = new Resource();
			$return = array();
			
			//根据搜索条件得出符合要求的资源信息
			$return['list'] = $resource->set_resource_list($start,$data);
			
			//根据搜索条件得出符合要求的资源总数
			$return['count'] = $resource->set_resource_count($data);
			
			//每页显示数量
			$return['pageSize'] = $pageSize;
			$return['types'] = $resource->map_types();//资源类型数组
			
			//页码(因为要跟ajax比对页码,防止事件驱动带来意想不到的BUG)
			$return['page'] = $page;
			
			//转换时间
			foreach($return['list'] as $k=>$v) {
				$return['list'][$k]['startime'] = date('Y-m-d H:i',$v['startime']);	
				$return['list'][$k]['endtime'] = date('Y-m-d H:i',$v['endtime']);	
				$return['list'][$k]['creatime'] = date('Y-m-d H:i',$v['creatime']);	
			}
			
			return Response::json(array('status'=>1,'info'=>'获取成功!','data'=>$return));
	}
	
	//添加商品资源关联
	public function relationResource() {
			$data = Request::all();
			$relation = new GoodsRelation();
			
			$relation_info = $relation->select_relation($data);
			if(!empty($relation_info)) {
				foreach($data['ids'] as $k=>$v) {
					
					//如果资源和商品已经有关联关系,则剔除
					if(isset($relation_info[$v])) {
						unset($data['ids'][$k]);	
					}	
				}	
			}
			
			//添加商品和资源的关系数据
			if($relation->insert_relation($data)) {
				return Response::json(array('status'=>1,'info'=>'操作成功!'));
			} else {
				return Response::json(array('status'=>0,'info'=>'操作失败!'));	
			}
	}
	
	//撤出资源
	public function optoutResource() {
		
			$data = Request::all();
			$relation = new GoodsRelation();
			
			//从关联表里删除关联关系
			if($relation->delete_relation($data)) {
				return Response::json(array('status'=>1,'info'=>'操作成功!'));
			} else {
				return Response::json(array('status'=>0,'info'=>'操作失败!'));	
			}
			
	}
	
	//批量撤出repeal_relation
	public function repealResource() {
			$data = Request::all();
			$relation = new GoodsRelation();
			
			//从关联表里删除关联关系
			if($relation->repeal_relation($data)) {
				return Response::json(array('status'=>1,'info'=>'操作成功!'));
			} else {
				return Response::json(array('status'=>0,'info'=>'操作失败!'));	
			}
				
	}
	
	//删除商品
	public function deleteGoods() {
			$id = (int)Request::input('goods_id');
			$goods = new Goods();
			
			//删除商品
			if($goods->delete_goods($id)) {
				return Response::json(array('status'=>1,'info'=>'删除成功!'));
			} else {
				return Response::json(array('status'=>0,'info'=>'删除失败!'));	
			}
	}
	
	//上下线
	public function lineGoods() {
			$data = Request::all();
			
			$id = (int)$data['goods_id'];
			$action = $data['handle'];
			$goods = new Goods();
			$relation = new GoodsRelation();
			
			$update = array();
			
			//如果是上线操作
			if($action == 'online') {
				$ids = $relation->get_ids($id);
				if(empty($ids)) return Response::json(array('status'=>0,'info'=>'此商品下没有资源,不能上线!'));
				
				//查询商品的上线时间,如果为0,则记录当前时间为上线时间
				$goods_info = $goods->goods_info($id);
				$update['online'] = 1;
				if(empty($goods_info['puawaytime'])) $update['puawaytime'] = time();
				
			//如果是下线操作
			} else if($action == 'offline') {
				$update['online'] = 0;	
			}
			
			//根据操作结果返回信息
			if($goods->line_goods($update,$id)) {
				return Response::json(array('status'=>1,'info'=>'操作成功!'));
			} else {
				return Response::json(array('status'=>0,'info'=>'操作失败!'));	
			}	
	}

	//审核商品
	public function checkGoods() {
			$id = (int)Request::input('goods_id');
			$good = new Goods();
			
			//修改商品的审核状态
			if($good->check_goods($id)) {
				return Response::json(array('status'=>1,'info'=>'审核成功!'));	
			} else {
				return Response::json(array('status'=>0,'info'=>'审核失败!'));	
			}
	}

	//取消审核商品
	public function repealGoods() {
			$id = (int)Request::input('goods_id');
			$good = new Goods();
			
			//取消商品的审核状态
			if($good->repeal_goods($id)) {
				return Response::json(array('status'=>1,'info'=>'取消审核成功!'));	
			} else {
				return Response::json(array('status'=>0,'info'=>'取消审核失败!'));	
			}
	}
	
	//plup采用上传流方式,所以要和编辑器的上传分开
	public function plup_upload() {
		//先在本地生成hash名字的图片,然后再curl上传到104服务器,最后再删除本地图片
		$common = new CommonController();
		$local = $common->plup_load();
		
		//如果上传失败,则直接返回信息
		if(empty($local['status'])) return Response::json(array('status'=>0,'info'=>'上传失败!'));
		
		//上传到104服务器
		$curl = $common->curl_upload($local['filepath'],'resource');
		
		//删除原来的文件
		@unlink($local['filepath']);
		
		//根据状态码返回信息
		if(empty($curl['status'])) {
			return Response::json(array('status'=>0,'info'=>'上传失败!'));
		} else {
			
			$img_url = $curl['filename'];
			//把图片地址存入cookie 5分钟
			Cookie::queue('goods_img', $img_url, 5);
			return Response::json(array('status'=>1,'info'=>'上传成功!','url'=>$img_url));
		}
	}
}