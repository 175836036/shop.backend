<?php 
namespace App\Http\Controllers;
use App\Models\Manage;
use Config,Session,Request,Response,Validator,Cookie;
use App\Libs\Helper;

class UserController extends Controller {
	
	//登录页面
	public function login()
	{
		
		return view('user.login')->with(array('username'=>Request::cookie('username')));
	}
	
	//检测登录
	public function _login() {

			//接收数据
			$data = Request::all();
			
			//验证表单数据合法
            $input = array(
                'username' => $data['username'],
                'password' => $data['password'],
				'verify' => $data['verify'],
            );
			//验证规则
			$rules = array(
				'username' => 'required',
                'password' => 'required',
				'verify' => 'required',
			);
			
			//如果有错误则返回
			$validator = Validator::make($input,$rules,array('required'=>':attribute 不能为空!'));
			if($validator->fails()) {
				$message = current($validator->messages()->toArray());
				return Response::json(array('status'=>0,'info'=>$message[0]));
			}
			//解码系统生成的验证码
			$code = Session::get('verify');
			
			//检查验证码
			if(strtolower($data['verify']) !== strtolower($code)) return Response::json(array('status'=>0,'info'=>'验证码错误!'));
			
			//检查用户是否存在
			$manager = new Manage();
			$manager_info = $manager->get_manager_info($data['username']);
			if(empty($manager_info)) return Response::json(array('status'=>0,'info'=>'用户不存在!'));
			
			//检查密码
			if(Helper::createMd5Pwd($data['password']) !== $manager_info['password']) return Response::json(array('status'=>0,'info'=>'密码错误!'));
			
			//如果有勾选记住密码,则把用户名计入
			if(!empty($data['remember'])) Cookie::queue('username', $data['username']);
			
			//把用户信息存入session
			$user_info = array(
				'uid'=>$manager_info['aid'],
				'username'=>$manager_info['username']
			);
			
			return Response::json(array('status'=>1,'info'=>'登录成功!','url'=>__ROOT__.'/admin/index'))->withCookie(Cookie::make('user_info',Helper::authcode(json_encode($user_info),'ENCODE')),120);
	}
	
	public function verify() {
		$verify = new \App\Libs\Verify(100,40,4,13);
		$verify->doimg();
		//Session::put('verify', strtolower($verify->getCode()));//写入session
		//$_SESSION['verify'] = strtolower($verify->getCode());	
	}
	
}
