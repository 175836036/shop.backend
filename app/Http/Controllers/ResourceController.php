<?php 
namespace App\Http\Controllers;
use App\Models\Resource;
use Config,Session,Request,Response,Validator,Cookie;
use App\Libs\Helper;

class ResourceController extends Controller {
	private $user_info;

	//初始化获取用户信息
	public function __construct() {
		$common = new CommonController();
		$this->user_info = $common->get_user_info();
	}

	//资源列表
	public function resourceList() {
		$resource = new Resource();
		Cookie::queue('resource_img', null , -1);
		return view('resource.list')->with(array('types'=>$resource->map_types()));
	}
	
	//ajax获取资源列表
	public function _resourceList() {
			//接受数据
			$data = Request::all();
			$page = (int)$data['page'];unset($data['page']);
			$pageSize = (int)config('params.pagesize');
			
			$start = ($page-1)*$pageSize;
			
			$resource = new Resource();
			$return = array();
			$return['list'] = $resource->search_resource_list($start,$data);
			$return['count'] = $resource->search_resource_count($data);
			$return['pageSize'] = $pageSize;
			$return['types'] = $resource->map_types();//资源类型数组
			$return['page'] = $page;
			
			//转换时间
			foreach($return['list'] as $k=>$v) {
				$return['list'][$k]['startime'] = date('Y-m-d H:i',$v['startime']);	
				$return['list'][$k]['endtime'] = date('Y-m-d H:i',$v['endtime']);	
				$return['list'][$k]['creatime'] = date('Y-m-d H:i',$v['creatime']);	
			}
			
			return Response::json(array('status'=>1,'info'=>'获取成功','data'=>$return));
	}

	

	//添加资源
	public function createResource() {
		$data = Request::all();
		//如果是ajax请求则将数据插入数据库
		if(!empty($data)) {
			$resource = new Resource();
			$id = $resource->create_resource($data,$this->user_info['uid']);

			if($id) {
				return Response::json(array('status'=>1,'info'=>'添加资源成功!'));
			} else {
				return Response::json(array('status'=>0,'info'=>'添加资源失败!'));
			}
		} else {
		
			//查询资源类型
			$model = new Resource();
			$resource_types = $model->resource_types();
			
			//传递数据到模板
			$data = array(
				'img_url'=>Cookie::get('resource_img'),
				'types'=>$resource_types
			);
			return view('resource.create')->with($data);
		}
	}

	//预览资源
	public function resourceDetail($id) {
		//接受ID
		$resource_id = (int)$id;

		//获取资源信息
		$resource = new Resource();
		$resource_info = $resource->get_resource_info($resource_id);
		
		//如果资源不存在则报错
		if(empty($resource_info)) {
			echo '资源不存在!';
			exit;

		//存在则查询资源类型信息
		} else {
			//获取资源类型信息(map结构)
			$type = $resource->map_types();
			return view('resource.detail')->with(array('data'=>$resource_info,'type'=>$type));
		}
	}
	
	//编辑资源
	public function resourceEdit($id) {
		//接受ID
		$resource_id = (int)$id;

		//获取资源信息
		$resource = new Resource();
		$resource_info = $resource->get_resource_info($resource_id);
		//如果资源不存在则报错
		if(empty($resource_info)) {
			echo '资源不存在!';
			exit;

		//存在则查询资源类型信息
		} else {
			//获取资源类型信息(map结构)
			$type = $resource->map_types();
			return view('resource.edit')->with(array('resource'=>$resource_info,'type'=>$type));
		}
	}
	
	//编辑页面(修改数据)
	public function _resourceEdit() {

			$data = Request::all();
			
			$resource = new Resource();
			$id = (int)$data['resource_id'];unset($data['resource_id']);
			
			if($resource->edit_resource($data,$this->user_info['uid'],$id)) {
				return Response::json(array('status'=>1,'info'=>'修改资源成功!'));
			} else {
				return Response::json(array('status'=>0,'info'=>'修改资源失败!'));
			}
	}
	
	//删除资源
	public function resourceDelete() {
		
			$id = (int)Request::input('resource_id');
			$resource = new Resource();
			if($resource->delete_resource($id)) {
				return Response::json(array('status'=>1,'info'=>'删除成功!'));	
			} else {
				return Response::json(array('status'=>0,'info'=>'删除失败!'));	
			}
	}
	
	//审核资源
	public function resourceCheck() {
			$id = (int)Request::input('resource_id');
			$resource = new Resource();
			if($resource->check_resource($id)) {
				return Response::json(array('status'=>1,'info'=>'审核成功!'));	
			} else {
				return Response::json(array('status'=>0,'info'=>'审核失败!'));	
			}
	}

	//取消审核
	public function resourceRepeal() {
			$id = (int)Request::input('resource_id');
			$resource = new Resource();
			
			if($resource->repeal_resource($id)) {
				return Response::json(array('status'=>1,'info'=>'取消审核成功!'));	
			} else {
				return Response::json(array('status'=>0,'info'=>'取消审核失败!'));	
			}
	}
	
	//plup采用上传流方式,所以要和编辑器的上传分开
	public function plup_upload() {
			//先在本地生成hash名字的图片,然后再curl上传到104服务器,最后再删除本地图片
			$common = new CommonController();
			$local = $common->plup_load();
		
			if(empty($local['status'])) return Response::json(array('status'=>0,'info'=>'上传失败!'));
			
			//上传到104服务器
			$curl = $common->curl_upload($local['filepath'],'resource');
			@unlink($local['filepath']);
			
			//根据状态码返回信息
			if(empty($curl['status'])) {
				return Response::json(array('status'=>0,'info'=>'上传失败!'));
			} else {
				$img_url = $curl['filename'];
				//把图片地址存入cookie 5分钟
				Cookie::queue('resource_img', $img_url, 5);
				return Response::json(array('status'=>1,'info'=>'上传成功!','url'=>$img_url));	
			}
	}

	
}