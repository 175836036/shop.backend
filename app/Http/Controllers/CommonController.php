<?php 
/*
comment:公共控制器(存放一些公共方法,比如上传)
author:hanyu
*/
namespace App\Http\Controllers;
use Config,Session,Request,Response,Validator,Cookie,DB;
use App\Libs\Helper;

class CommonController extends Controller {
	
	//上传资源图片(plup插件上传处理)
	public function plup_load() {
			// 5 minutes execution time
			@set_time_limit(5 * 60);
			
			
			// 获取php.ini里配置的上传临时目录
			$targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR;
			$cleanupTargetDir = true; // Remove old files
			$maxFileAge = 5 * 3600; // Temp file age in seconds
			
			// 如果没有此目录,则创建
			if (!file_exists($targetDir)) {
				mkdir($targetDir);
			}
			
			// 获取文件名
			if (isset($_REQUEST["name"])) {
				$fileName = $_REQUEST["name"];
			} elseif (!empty($_FILES)) {
				$fileName = $_FILES["file"]["name"];
			} else {
				$fileName = uniqid("file_");
			}
			
			$filePath = $targetDir. $fileName;
			
			// Chunking might be enabled
			$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
			$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
			// Remove old temp files	
			if ($cleanupTargetDir) {
				if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
					return array('status'=>0,'info'=>'上传失败!');
				}
				//遍历文件夹
				while (($file = readdir($dir)) !== false) {
					$tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;
					// If temp file is current file proceed to the next
					if ($tmpfilePath == "{$filePath}.part") {
						continue;
					}
			
					// Remove temp file if it is older than the max age and is not the current file
					if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
						@unlink($tmpfilePath);
					}
				}
				closedir($dir);
			}
			
			// Open temp file
			if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
				return array('status'=>0,'info'=>'上传失败!');
			}
			if (!empty($_FILES)) {
				if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
					return array('status'=>0,'info'=>'上传失败!');
				}
				// Read binary input stream and append it to temp file
				if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
					return array('status'=>0,'info'=>'上传失败!');
				}
			} else {	
				if (!$in = @fopen("php://input", "rb")) {
					return array('status'=>0,'info'=>'上传失败!');
				}
			}
			
			while ($buff = fread($in, 4096)) {
				fwrite($out, $buff);
			}
			
			@fclose($out);
			@fclose($in);
			
			//计算新的文件名
			$path = dirname($filePath).DIRECTORY_SEPARATOR;
			$file_arr = explode('.',basename($filePath));
			
			//获取后缀名
			$Exts=end($file_arr);
			
			//计算盐值
			$salt = dechex(microtime(true));
			
			//新文件的名字
			$filename=md5(time().$salt).'.'.$Exts;
			$new_path = $path.$filename;
			
			// 把.part文件改名为新文件的名字
			if (!$chunks || $chunk == $chunks - 1) {
				rename("{$filePath}.part",$new_path);
			}
			
			return array('status'=>1,'info'=>'上传成功!','filepath'=>$new_path);
	}
	
	//商品和资源公用的上传方法
	public function curl_upload($filePath) {
			//获取ftp配置信息
			$ftp_config = Config::get('params.ftp_config');
			
			//获取文件名,并分割成数组
			$name = basename($filePath);
			$file_arr = explode('.',$name);
			
			//获取后缀
			$Ext = end($file_arr);
			
			//去除掉图片名中的斜杠
			$hash = sha1(file_get_contents($filePath));
			
			//取前两位为文件夹名
			$dir = substr($hash,0,2);
			
			//剩余的为文件名
			$filename = substr($hash,2).'.'.$Ext;
			
			//上传目录
			$upload_url = $ftp_config['server'].'zhuantiku/media/images/'.$dir.'/'.$filename;
			
			//初始化curl
			$ch = curl_init();
			
			//获取临时文件的资源句柄
			$fp=fopen($filePath,'r');
			
			//设置请求的url地址
			curl_setopt($ch, CURLOPT_URL,$upload_url);
			curl_setopt($ch, CURLOPT_USERPWD, "{$ftp_config['user']}:{$ftp_config['pass']}");//设置ftp的用户密码
			curl_setopt($ch, CURLOPT_PUT,1);//使用http上传一个文件
			curl_setopt($ch, CURLOPT_UPLOAD, 1);//curl执行时上传一个文件
			curl_setopt($ch, CURLOPT_INFILE, $fp);//要上传的文件
			curl_setopt($ch, CURLOPT_FTP_CREATE_MISSING_DIRS,1);//如果目录不存在则创建
			curl_exec($ch);//执行
			$error_no = curl_errno($ch);//获取错误码
			
			//错误码等于0的时候代表上传成功
			if($error_no==0){
				return array('status'=>1, 'filename'=> config('params.img_url').$dir.'/'.$filename);
			}else{
				return array('status'=>0, 'info'=>'图片上传失败!');
			}
	}
	
	//编辑器上传图片
	public function ueditor_upload() {
			//接受上传的图片信息
			$file = $_FILES['upfile'];
			
			//获取文件名,并分割成数组
			$file_arr = explode('.',$file['name']);
			$Ext = end($file_arr);unset($file_arr);
			
			//验证后缀名
			if(!in_array($Ext,array('jpg','jpeg','png'))) return ;
			
			//获取临时文件的名字
			$filePath = $file['tmp_name'];
			
			//去除掉图片名中的斜杠
			$hash = sha1(file_get_contents($filePath));
			
			//取前两位为文件夹名
			$dir = substr($hash,0,2);
			
			//剩余的为文件名
			$filename = substr($hash,2).'.'.$Ext;
		
			//获取ftp配置信息
			$ftp_config = Config::get('params.ftp_config');
			$upload_url = $ftp_config['server'].'zhuantiku/media/images/'.$dir.'/'.$filename;
			
			//初始化curl
			$ch = curl_init();
			
			//获取临时文件的资源句柄
			$fp=fopen($filePath,'r');
			
			//设置请求的url地址
			curl_setopt($ch, CURLOPT_URL,$upload_url );
			curl_setopt($ch, CURLOPT_USERPWD, "{$ftp_config['user']}:{$ftp_config['pass']}");//设置ftp的用户密码
			curl_setopt($ch, CURLOPT_PUT,1);//使用http上传一个文件
			curl_setopt($ch, CURLOPT_UPLOAD, 1);//curl执行时上传一个文件
			curl_setopt($ch, CURLOPT_INFILE, $fp);//要上传的文件
			curl_setopt($ch, CURLOPT_FTP_CREATE_MISSING_DIRS,1);//如果目录不存在则创建
			curl_exec($ch);//执行
			$error_no = curl_errno($ch);//获取错误码
			
			//错误码等于0的时候代表上传成功
			if($error_no==0){
				echo json_encode(array('state'=>'SUCCESS','title'=>$file['name'],'url'=>$dir.'/'.$filename));
			}else{
				echo json_encode(array('state'=>'ERROR','info'=>'上传失败!'));
			}
	}
	
	//根据用户名查询UC里是否存在这个用户
	public function checkUser() {
		//接受用户名
		$username = Request::input('username');
		
		//查询用户是否存在
		$sql = "SELECT uid,username FROM uc_members WHERE username=? LIMIT 1";
		$user_info = DB::connection('uc_user')->select($sql,[$username]);
		
		//关闭连接
		DB::disconnect('uc_user');	
		
		if(empty($user_info)) {
			return Response::json(array('status'=>0,'info'=>'用户不存在!'));
		} else {
			return Response::json(array('status'=>1,'info'=>'存在此用户!','data'=>$user_info[0]));
		}
	}


	//获取用户信息
	public function get_user_info() {
		//如果cookie里有user)info
		if(Cookie::has('user_info')) {
			//解码数据
			$user_info = json_decode(Helper::authcode(Cookie::get('user_info'),'DECODE'),true);
			
			//如果uid不存在,则返回false
			if(!isset($user_info['uid']) || empty($user_info['uid'])) {
				return false;
			//否则返回用户信息
			}else {
				return $user_info;
			}
			
		//cookie不存在则返回false
		} else {
			return false;
		}
		
	}
}