<?php namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 * 全局中间件(每次请求页面都会执行的中间件)
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
	];

	/**
	 * The application's route middleware.
	 * 制定中间件(根据key指定给需要的路由)
	 * @var array
	 */
	protected $routeMiddleware = [
		'is_login' => 'App\Http\Middleware\LoginMiddleware',//检查是否登录
		'check_login'=>'App\Http\Middleware\UserMiddleware',//后台检查是否登录
		'check_role'=>'App\Http\Middleware\RoleMiddleware',//检查后台权限
	];

}
