华图在线商城项目后端
==========================

:Author: 韩钰
:Date: 08/01 2015

.. contents::

介绍
-----------

华图在线商城项目后端.

部署
-------------

.. code:: bash

    docker build -t luozijun/shop.backend .
    docker run -d -p 80:80 luozijun/shop.backend 


技术
-----------

*   `PHP56 <https://secure.php.net/downloads.php#v5.6.13>`_
*   `Laravel PHP Framework <http://laravel.com/docs/5.1>`_

依赖
------------

