@include('public.header')
<script type="text/javascript" src="{{__STATIC__}}/js/plupload/plupload.full.min.js"></script>
<script type="text/javascript" src="{{__STATIC__}}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="{{__STATIC__}}/css/jquery.datetimepicker.css"/>
<title>邮寄订单列表</title>
</head>
<body>
    <div id="container">
    	<div id="header">
            <h2 class="title">邮寄订单列表</h2>
            <div class="group_button">
               
            </div>
        </div>
        <div id="content">
        	<div id = "handle">
            	<div class="left_group">
                	<div class="order_time fl">
                        <label>下单时间</label>
                        <input type="text" class="text" readonly id="startime"/> - <input type="text" class="text" readonly id="endtime"/>
                    </div>
                    <select class="select" name='search_field'>
                        <option value="order_number">订单号</option>
                        <option value="username">用户名</option>
                        <option value="telephone">联系电话</option>
                    </select>
                    <input type="text" class="search_text text"/>
                    <select class="select mail" name="mail">
                        <option value="">全部</option>
                        <option value="0">未邮寄</option>
                        <option value="1">已邮寄</option>
                    </select>
                    <button type="button" id="search">搜索</button>
                    <button type="button" id="export">报表导出</button>
                    <button type="button" id="import">导入邮寄信息</button>
                    <a href="{{__STATIC__}}/file/mail_order_tpl.xlsx" class="download_url">邮寄模板下载</a>
                </div>
            </div>
            <div class="list">
                <table>
                    <thead>
                    	<tr>
                        	<th>选择</th>
                            <th>订单ID</th>
                            <th>订单号</th>
                            <th>用户名</th>
                            <th>联系电话</th>
                            <th>订单类型</th>
                            <th>订单金额</th>
                            <th>下单时间</th>
                            <th>状态</th>
                            <th class="handle">操作</th>
                        <tr>
                    </thead>
                    <tbody>
                       
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<td><input type="checkbox" id="all"/></td>
                        	<td colspan="11">
                            	<div class="email_handle">
                                    <label for="all">全选</label>
                                    <button class="disabled" id="create_mail">添加邮寄订单</button>
                                    <button class="disabled" id="delete_mail">删除邮寄订单</button>
                                </div>
                            	<div class="page fr">
                                    <div class="page_info">共1条记录,每页10条,共1页</div>
                                    <div class="page_handle">
                                        跳转至第 <input type="text" value="1" class="page"/> 页,页数<span class="num">1/1</span>
                                        <button class="prev"> < </button>
                                        <button class="next"> > </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <form action="{{__ROOT__}}/admin/exportMailExcel" method="post">
        <input type="hidden" name="export_data" value=''/>
    </form>
<div id="dialog">
	
</div>
<div id="change_list">
      <div class="header">
      		<h2>商品列表</h2>
            <div class="group_button">
                <button class="cancel">取消</button>
                <button class="confirm" id="save_mail">确定</button>
            </div>
      </div>
      <div class="content">
      		
      </div>
</div>
<script>
	//插件ajax上传
	window.uploader_config = {
		url:'{{__ROOT__}}/admin/importMailExcel',
		flash_swf_url:'{{__ROOT__}}/js/plupload/Moxie.swf',
		extensions:"xlsx"
	};
	import_mail('import');
	window.URL = {
		'list_url':"{{__ROOT__}}/admin/mailList",
		'detail_url':"{{__ROOT__}}/admin/maildetail/",
        'delete_url':"{{__ROOT__}}/admin/deleteMail",
        'update_url':"{{__ROOT__}}/admin/updateMail"
	};
	$('#startime,#endtime').datetimepicker({
			lang:'ch',
			timepicker:true,//是否精确到时分秒
			format:'Y-m-d H:i',
			formatDate:'Y-m-d',
			formatTime:'H:i',
            step:5,
			validateOnBlur:false//失去焦点不保存日期
	});
	mail_list($,window);
</script>
</body>
</html>