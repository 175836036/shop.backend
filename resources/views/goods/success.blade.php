@include('public.header')
<title>添加商品</title>
</head>
<body>
    <div id="container">
    	<div id="header">
            <h2 class="title">添加商品</h2>
            <ul class="plan">
                <li>1.创建商品</li>
                <li>2.添加资源</li>
                <li>3.商品预览</li>
                <li class="active">4.提交审核</li>
            </ul>
            <div class="group_button">
                <a class="cancel">上一步</a>
                <a href="{{__ROOT__}}/admin/goodsList" class="confirm">完成</a>
            </div>
        </div>
        <div id="content">
        	<div class="success">
            	<h1>您的商品已提交成功,请等待审核</h1>
            </div>
        </div>
    </div>
</body>
</html>