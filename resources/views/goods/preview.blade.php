<?php 
use App\Libs\Helper;
?>
@include('public.header')
<title>添加商品</title>
</head>
<body>
    <div id="container">
    	<div id="header">
            <h2 class="title">添加商品</h2>
            <ul class="plan">
                <li>1.创建商品</li>
                <li>2.添加资源</li>
                <li class="active">3.商品预览</li>
                <li>4.提交审核</li>
            </ul>
            <div class="group_button">
                <a class="cancel">上一步</a>
                <a href= "{{__ROOT__}}/admin/Goodssuccess" class="confirm" id="submit_checker">提交审核</a>
            </div>
        </div>
        <div id="content">
        		<div class="form_container">
                        <div class="form_div">
                            <div class="form_list first">
                                <label class="name">商品名称</label>
                                <div class="info">{{$goods_info['name']}}</div>
                            </div>
                            <div class="form_list">
                                <label class="name">商品价格</label>
                                <div class="info">{{$goods_info['price']}}</div>
                            </div>
                            <div class="form_list">
                                <label class="name">课时</label>
                                <div class="info">{{$goods_info['hours']}}</div>
                            </div>
                            <div class="form_list">
                                <label class="name">限购数量</label>
                                <div class="info">
                                	@if(!empty($goods_info['quota_num']))
                                    	{{$goods_info['quota_num']}}
                                    @else
                                    	不限
                                    @endif
                                </div>
                            </div>
                            <div class="form_list">
                                <label class="name">有效期</label>
                                <div class="time_info">
                                    <div class="startime"><?php echo date('Y-m-d H:i',$goods_info['startime']);?></div> 至
                                    <div class="endtime"><?php echo date('Y-m-d H:i',$goods_info['endtime']);?></div>
                                </div>
                            </div>
                            <div class="form_list">
                                <label class="name">是否邮寄</label>
                                <div class="info">
                                	@if(!empty($goods_info['mail']))
                                    	是
                                    @else
                                    	否
                                    @endif
                                </div>
                            </div>
                            <div class="form_list">
                                <label class="name">是否置顶</label>
                                <div class="info">
                                	@if(!empty($goods_info['stick']))
                                    	是
                                    @else
                                    	否
                                    @endif
                                </div>
                            </div>
                        </div>
                 
                        <div class="upload_div">
                            <div class="img">
                            	<a href="{{$goods_info['cover_url']}}" target="_blank">
                                	<img src="{{$goods_info['cover_url']}}" />
                                </a>
                            </div>
                        </div>
                        <div class = "attr_list">
                            <div class="form_list" >
                                <label class="name">商品扩展</label>
                                @if(isset($goods_info['attrs_list']) && !empty($goods_info['attrs_list']))
                                    <ul class="resource_button">
                                        <?php $i = 1;?>
                                            @foreach($goods_info['attrs_list'] as $k=>$v)
                                                <li class="<?php if($i==1) echo 'first active';?>" data-name="{{$k}}" data-content="{{$v}}"><?php echo $i;?></li>
                                               <?php $i++;?>
                                            @endforeach
                                    </ul>
                                @else
                                	<div class="info">无</div>
                                @endif
                            </div>
                        </div>
                        @if(isset($goods_info['attrs_list']) && !empty($goods_info['attrs_list']))
                            <div class="attr_name">
                                <h2>名称</h2>
                                <div class="name_detail"><?php echo key($goods_info['attrs_list']);?></div>
                            </div>
                            <div class="editor_div">
                                <div id="attr_detail">
                                    <?php echo current($goods_info['attrs_list']);?>
                                </div>
                            </div>
                        @endif
                        <div class="resource_detail">
                            <label class="name">资源列表</label>
                            <div class="resource_list">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>序号</th>
                                            <th>资源名称</th>
                                            <th>资源类型</th>
                                            <th>作者(老师)</th>
                                            <th>有效期</th>
                                            <th>创建时间</th>
                                            <th>操作</th>
                                        <tr>
                                    </thead>
                                    	@if(isset($resource_info) && !empty($resource_info))
                                            <tbody>
                                                @foreach($resource_info as $k=>$v)
                                                <tr>
                                                    <td>{{$v['resource_id']}}</td>
                                                    <td title="{{$v['name']}}">{{Helper::aaa($v['name'],15)}}</td>
                                                    <td>{{$types[$v['resource_type']]}}</td>
                                                    <td title="{{$v['author']}}">{{Helper::aaa($v['author'],4)}}</td>
                                                    <td><?php echo date('Y-m-d H:i',$v['startime']);?>至<?php echo date('Y-m-d H:i',$v['endtime']);?></td>
                                                    <td><?php echo date('Y-m-d H:i',$v['creatime']);?></td>
                                                    <td>
                                                        <a href="{{__ROOT__}}/admin/resourceDetail/{{$v['resource_id']}}" class="detail">查看</a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            <!--<tfoot>
                                                <tr>
                                                    <td colspan="7">
                                                        <div class="page">
                                                            <div class="page_info">共0条记录,每页1条,共1页</div>
                                                            <div class="page_handle">
                                                                跳转至第 <input type="text" value="5"/> 页,<span class="num">页数12/20</span>
                                                                <button class="prev"> < </button>
                                                                <button class="next"> > </button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tfoot>-->
                                        @else
                                        	<tfoot>
                                                <tr>
                                                    <td colspan="7" align="center">
                                                        无资源
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        @endif
                                </table>
                            </div>
                        </div>
            	</div>
        </div>
    </div>
</body>
<script>
	 preview($,window);
</script>
</html>