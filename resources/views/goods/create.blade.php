@include('public.header')
<script type="text/javascript" src="{{__STATIC__}}/js/plupload/plupload.full.min.js"></script>
<script type="text/javascript" src="{{__STATIC__}}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="{{__STATIC__}}/css/jquery.datetimepicker.css"/>
<title>添加商品</title>
</head>
<body>
    <div id="container">
    	<div id="header">
            <h2 class="title">添加商品</h2>
            <ul class="plan">
                <li class="active">1.创建商品</li>
                <li>2.添加资源</li>
                <li>3.商品预览</li>
                <li>4.提交审核</li>
            </ul>
            <div class="group_button">
                <a class="cancel" data-url="{{__ROOT__}}/admin/goodsList">取消</a>
                <button type="button" class="confirm" id="change_resource" data-url="{{__ROOT__}}/admin/createGoods" >添加资源</button>
            </div>
        </div>
        <div id="content">
        	<div class="form_container">
                <div class="form_div">
                    <div class="form_list first">
                        <label class="name">商品名称</label>
                        <input type="text" class="text" name="goods_name"/>
                    </div>
                    <div class="form_list">
                        <label class="name">商品价格</label>
                        <input type="text" class="text" name="price"/>
                    </div>
                    <div class="form_list">
                        <label class="name">课时</label>
                        <input type="text" class="text" name="hours"/>
                    </div>
                    <div class="form_list">
                        <label class="name">限购数量</label>
                        <input type="text" class="text" name="quota_num"/>
                    </div>
                    <div class="form_list">
                        <label class="name">销量基数</label>
                        <input type="text" class="text" name="base_number"/>
                    </div>
                    <div class="form_list">
                        <label class="name">有效期</label>
                        <input type="text" id="startime" class="time" name="startime"/> 至
                       	<input type="text" id="endtime" class="time" name="endtime"/>
                    </div>
                    <div class="form_list">
                        <label class="name">是否邮寄</label>
                        <input type="radio" name="mail" class="radio" value="1"/> <label class="radio_name">是</label>
                        <input type="radio" name="mail" class="radio false" value="0" checked="checked"/> <label class="radio_name">否</label>
                    </div>
                    <div class="form_list">
                        <label class="name">是否置顶</label>
                        <input type="radio" name="stick" class="radio" value="1"/> <label class="radio_name">是</label>
                        <input type="radio" name="stick" class="radio false" value="0" checked="checked"/> <label class="radio_name">否</label>
                    </div>
                    <div class="form_list">
                        <label class="name">所属分类</label>
                        <select class="select" name="nav">
                            <option value="0">请选择所属分类</option>
                            @if(isset($nav_info) && !empty($nav_info))
                                @foreach($nav_info as $v)
                                <option value="{{$v['nav_id']}}">{{$v['name']}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form_list">
                        <label class="name">商品说明</label>
                        <input type="text" class="text" name="introduce"/>
                    </div>
                </div>
         
                <div class="upload_div">
                    <div class="img">
                    	@if(isset($img_url) && !empty($img_url))
                        	<a href="{{$img_url}}" target="_blank">
                            	<img src="{{$img_url}}"/>
                            </a>
                        @else
                        	图片预览
                        @endif
                    </div>
                    <div class="handle">
                        <button id="change_img" data-coverurl = "{{$img_url}}">选择图片</button>
                        <p>
                            (图片格式为png、jpg)
                        </p>
					</div>
                    <div class="error_div"></div>
                </div>
                <div class = "attr_list">
                	<div class="form_list" >
                        <label class="name">商品扩展</label>
                        <ul class="resource_button">
                        	<li class="first active">1</li>
                        </ul>
                        <a id="add_attr">+新增扩展</a>
                        <a id="remove_attr">-删除扩展</a>
                    </div>
                    <div class="error_list">
                    	<div class="error_div"></div>
                    </div>
                </div>
                <div class="attr_name">
                    <h2>名称</h2>
                    <div class="name" placeholder="请输入名称">请输入名称</div>
                    <textarea type="text" class="name" placeholder="可手工输入名字,鼠标单击为编辑状态,点击其他地方保存"></textarea>
                </div>
                <div class="editor_div">
                	<textarea id = "attr_content"></textarea>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
	$(function () {
		var myDate = new Date();
    	//插件ajax上传
		window.uploader_config = {
			url:'{{__ROOT__}}/admin/goods_cover',
			flash_swf_url:'{{__ROOT__}}/js/plupload/Moxie.swf',
			extensions:"jpg,jpeg,png",
		};
		plup_load();
		
		$('input[name=startime],input[name=endtime]').datetimepicker({
			lang:'ch',
			timepicker:true,//是否精确到时分秒
			format:'Y-m-d H:i',
			formatDate:'Y-m-d',
			formatTime:'H:i',
            step:5,
			validateOnBlur:false,//失去焦点不保存日期
			yearStart:myDate.getFullYear(),
			minDate:String(myDate.getDate()),
		});
		
		//实例化编辑器
		window.UMEDITOR_HOME_URL = "{{__STATIC__}}/js/ueditor/";  //UEDITOR_HOME_URL、config、all这三个顺序不能改变
       	window.UMEDITOR_CONFIG.initialFrameHeight=300;//编辑器的高度
       	window.UMEDITOR_CONFIG.imageUrl="{{__ROOT__}}/admin/ueditor";          //图片上传提交地址
       	window.UMEDITOR_CONFIG.imagePath=' <?php echo config('params.img_url');?>';//编辑器调用图片的地址
	   	window.UMEDITOR_CONFIG.initialFrameWidth = 748;
	   	window.UMEDITOR_CONFIG.initialFrameHeight = 200;
		
		window.URL = {
			change_resource : "{{__ROOT__}}/admin/changeResource/"
		}
	   	add_goods($,window);
   })

</script>
<script type="text/javascript" charset="utf-8" src="{{__STATIC__}}/js/ueditor/umeditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="{{__STATIC__}}/js/ueditor/umeditor.min.js"></script>
</html>