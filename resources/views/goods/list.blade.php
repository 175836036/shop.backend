@include('public.header')
<title>商品列表</title>
</head>
<body>
    <div id="container">
    	<div id="header">
            <h2 class="title">商品列表</h2>
            <div class="group_button">
                <a href="{{__ROOT__}}/admin/createGoods" class="add">添加商品</a>
            </div>
        </div>
        <div id="content">
        	<div id = "handle">
            	<div class="left_group">
                    <select class="select" name="search_field">
                        <option value = "goods_id">商品ID</option>
                        <option value = "name">商品名称</option>
                    </select>
                    <input type="text" class="search_text text"/>
                    <button type="button" id="search">搜索</button>
                </div>
            </div>
            <div class="list">
                <table>
                    <thead>
                    	<tr>
                            <th>商品ID</th>
                            <th>商品名称</th>
                            <th>价格</th>
                            <th>有效期</th>
                            <th>创建时间</th>
                            <th>审核状态</th>
                            <th>状态</th>
                            <th class="handle">操作</th>
                        <tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<td colspan="8">
                            	<div class="page">
                                    <div class="page_info">共0条记录,每页1条,共1页</div>
                                    <div class="page_handle">
                                        跳转至第 <input class="page" type="text"/> 页,页数<span class="num">12/20</span>
                                        <button class="prev"> < </button>
                                        <button class="next"> > </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</body>
<script>
	window.URL = {
		'goodsList':"{{__ROOT__}}/admin/goodsList",
		'goodsdetail':"{{__ROOT__}}/admin/goodsdetail/",
		'edit_url' : "{{__ROOT__}}/admin/editGoods/",
		'delete_url' : "{{__ROOT__}}/admin/deleteGoods",
		'line_url':"{{__ROOT__}}/admin/lineGoods",
        'check_url':"{{__ROOT__}}/admin/checkGoods",
        'cancel_url':"{{__ROOT__}}/admin/repealGoods"
	}
	
	goods_list($,window);
</script>
</html>