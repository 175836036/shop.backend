@include('public.header')
<title>订单详情</title>
</head>
<body>
    <div id="container">
    	<div id="header">
            <h2 class="title">订单详情</h2>
            <div class="group_button">
                <button type="button" class="@if($index == 2) disabled @else delete @endif" data-id="{{$order_info['order_id']}}" data-index = '{{$index}}'>取消订单</button>
                <a type="button" class="confirm" href="{{__ROOT__}}/admin/orderList">返回列表</a>
            </div>
        </div>
        <?php 
			$pattern = array(
				1 => '支付宝',
				2 => '微信',
				3 => '免支付',
			);
			$form = array(
				1 => 'web',
				2 => 'app',
				3 => 'wap',
				4 => '其他',
			);
		?>
        <div id="content">
        	<div class="form_container">
                <div class="detail_list">
                	<div class="detail_row">
                    	<div class="name">订单号</div>
                        <div class="detail">{{$order_info['order_number']}}</div>
                    </div>
                    <div class="detail_row">
                    	<div class="name">订单类型</div>
                        <div class="detail">
                            @if($order_info['is_system'] == 1)
                            	系统订单
                            @else
                            	@if($order_info['is_give'] == 1)
                                手工订单(赠送)
                                @else
                                手工订单
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="detail_row">
                        <div class="name">商品名称</div>
                        <div class="detail">
                            <ul class="goods_detail">
                                @if(isset($snap_info) && !empty($snap_info))
                                    @foreach($snap_info as $v)
                                        <li>{{$v['name']}}</li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="detail_row">
                    	<div class="name">用户名</div>
                        <div class="detail">{{$order_info['username']}}</div>
                    </div>
                    
                    @if(isset($mail_info) && !empty($mail_info))
                        <div class="detail_row">
                            <div class="name">收件人</div>
                            <div class="detail">{{$mail_info['receiver']}}</div>
                        </div>
                        <div class="detail_row">
                            <div class="name">联系电话</div>
                            <div class="detail">{{$mail_info['telephone']}}</div>
                        </div>
                        <div class="detail_row">
                            <div class="name">收件地址</div>
                            <div class="detail">{{$mail_info['address']}}</div>
                        </div>
                    @endif
                    <div class="detail_row">
                    	<div class="name">下单时间</div>
                        <div class="detail"><?php echo date('Y-m-d H:i',$order_info['orders_time']);?></div>
                    </div>
                    <div class="detail_row">
                    	<div class="name">订单金额</div>
                        <div class="detail">{{$order_info['amount']}}</div>
                    </div>
                    <div class="detail_row">
                    	<div class="name">支付状态</div>
                        <div class="detail">  	
                            @if($order_info['pay_status'] == 1)
                            	已支付
                            @else
                            	未支付
                            @endif
                        </div>
                    </div>
                    <div class="detail_row">
                    	<div class="name">支付方式</div>
                        <div class="detail">{{$pattern[$order_info['pattern']]}}</div>
                    </div>
                    <div class="detail_row">
                    	<div class="name">购买来源</div>
                        <div class="detail">{{$form[$order_info['form']]}}</div>
                    </div>
                    <div class="detail_row">
                    	<div class="name">订单说明</div>
                        <div class="detail">
                        @if(!empty($order_info['order_explain']))
                        	{{$order_info['order_explain']}}
                        @else
                        	暂无
                        @endif
                        </div>
                    </div>
                    @if(isset($mail_info))
                        @if(!empty($mail_info['company_name']))
                        <div class="detail_row">
                            <div class="name">快递公司</div>
                            <div class="detail">{{$mail_info['company_name']}}</div>
                        </div>
                        @endif
                        @if(!empty($mail_info['company_id']))
                        <div class="detail_row">
                            <div class="name">快递单号</div>
                            <div class="detail">{{$mail_info['company_num']}}</div>
                        </div>
                        @endif
                    @endif
                </div>  
            </div>
        </div>
    </div>
</body>
<script>
	window.URL = {
		'delete_url':"{{__ROOT__}}/admin/deleteOrder"	
	}
	//取消订单
	$('.group_button').on('click','.delete',function () {
		var _this = $(this);
		if(confirm('确定要取消此订单吗?')) {
			$.post(URL.delete_url,{id:_this.data('id'),index:_this.data('index')},function (res) {
				if(res.status) {
					window.history.back()
				} else {
					alert(res.info);	
				}
			})
		}
	})
</script>
</html>