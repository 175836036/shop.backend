@include('public.header')
<script type="text/javascript" src="{{__STATIC__}}/js/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="{{__STATIC__}}/css/jquery.datetimepicker.css"/>
<title>订单列表</title>
</head>
<body>
    <div id="container">
    	<div id="header">
            <h2 class="title">订单列表</h2>
            <ul class="order_tab">
                <li class="active init" title="手工代报订单和未到账订单">初始订单</li>
                <li>已审核订单</li>
                <li>已删除订单</li>
            </ul>
            <div class="group_button">
                <a class="add" href="{{__ROOT__}}/admin/createOrder">添加订单</a>
            </div>
        </div>
        <div id="content">
        	<div id = "handle">
            	<div class="left_group">
                    <select class="select" name='search_field'>
                        <option value="order_number">订单号</option>
                        <option value="username">用户名</option>
                    </select>
                    <input type="text" class="search_text text"/>
                    <div class="order_time none">
                        <label>下单时间</label>
                        <input type="text" class="text" readonly id="startime"/> - <input type="text" class="text" readonly id="endtime"/>
                    </div>
                    <div class="amount none">
                        <label>订单金额</label>
                        <input type="text" class="text" id="min_amount"/> - <input type="text" class="text" id="max_amount"/>
                    </div>
                    <div class="fl more">
                        <select class="select pattern none" name="pattern">
                            <option value="0">支付方式</option>
                            <option value="1">支付宝</option>
                            <option value="2">微信</option>
                        </select>
                        <select class="select form none" name="form">
                            <option value="0">购买来源</option>
                            <option value="1">web</option>
                            <option value="2">app</option>
                            <option value="3">wap</option>
                        </select>
                        <button type="button" id="search">搜索</button>
                    	<button type="button" id="more">更多筛选</button>
                    	<button type="button" id="export">报表导出</button>
                    </div>
                </div>
            </div>
            <div class="list">
                <table>
                    <thead>
                    	<tr>
                            <th>序号</th>
                            <th>订单号</th>
                            <th>用户名</th>
                            <!--<th>手机号/邮箱</th>-->
                            <th>订单类型</th>
                            <th>订单金额</th>
                            <th>下单时间</th>
                            <th>支付方式</th>
                            <th>购买来源</th>
                            <th class="handle">操作</th>
                        <tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<td colspan="11">
                            	<div class="page">
                                    <div class="page_info">共条记录,每页10条,共1页</div>
                                    <div class="page_handle">
                                        跳转至第 <input type="text" value="1" class="page"/> 页,页数<span class="num">1/1</span>
                                        <button class="prev"> < </button>
                                        <button class="next"> > </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <form action="{{__ROOT__}}/admin/exportExcel" method="post">
    	<input type="hidden" name="export_data" value=''/>
    </form>
</body>
<script>
	window.URL = {
		list_url:"{{__ROOT__}}/admin/orderList",
		detail_url:"{{__ROOT__}}/admin/orderdetail/",
		restore_url:"{{__ROOT__}}/admin/restoreOrder",
		check_url:"{{__ROOT__}}/admin/checkOrder",
		delete_url:"{{__ROOT__}}/admin/deleteOrder",
		cancel_url:"{{__ROOT__}}/admin/cancelOrder",
		edit_url:"{{__ROOT__}}/admin/editOrder/"
	};
	$('#startime,#endtime').datetimepicker({
			lang:'ch',
			timepicker:true,//是否精确到时分秒
			format:'Y-m-d H:i',
			formatDate:'Y-m-d',
			formatTime:'H:i',
            step:5,
			validateOnBlur:false//失去焦点不保存日期
	});
	order_list($,window);
</script>
</html>