<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="{{__STATIC__}}/css/common.css">
<link rel="stylesheet" type="text/css" href="{{__STATIC__}}/css/index.css">
<script type="text/javascript" src="{{__STATIC__}}/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="{{__STATIC__}}/js/common.js"></script>
<script>
window.PUBLIC = "{{__STATIC__}}/images";
</script>
<script type="text/javascript" src="{{__STATIC__}}/js/index.js"></script>
<title>后台首页</title>
</head>
<body>
	<div id = "header">
    	<div class="logo">
        	<div class="yuan"></div>
            <a href="{{__ROOT__}}/admin/index">
            	<h1>商城系统</h1>
            </a>
        </div>
        <div class="shop_info">
            <img src = "{{__STATIC__}}/images/stat.png"/>
            <div class="stat_info">历史总收入:{{$total_amount}}元</div>
            <div class="stat_info today_amount">今日收入:{{$today_amount}}元</div>
            <div class="title">今日收入=已审核订单金额之和</div>
        </div>
        <div class="user_div">
            <div class="user_info">
                <span>{{$username}}</span>
                <img src="{{__STATIC__}}/images/user_down.png"/>
            </div>
            <ul class="menu">
                <a href='{{__ROOT__}}/admin/updatePwd' target="main"><li>修改密码</li></a>
                <a href='{{__ROOT__}}/admin/logout'><li>退出</li></a>
            </ul>
        </div>
    </div>
    <div id = "sidebar" class="sidebar_show">
        <ul class="side_menu">
            <li>
                <div class="parent">
                    <img src = "{{__STATIC__}}/images/menu/resource_normal.png" class = "icon"/>
                        <span>资源中心</span>
                    <img src = "{{__STATIC__}}/images/menu/direction_right.png" class = "direction"/>
                </div>
                <dt class="son">
                    <a class="url" href="{{__ROOT__}}/admin/resourceList" target="main">
                        资源列表
                    </a>
                </dt>
            </li>
            <li>
                <div class="parent">
                    <img src = "{{__STATIC__}}/images/menu/goods_normal.png" class = "icon"/>
                        <span>商品中心</span>
                    <img src = "{{__STATIC__}}/images/menu/direction_right.png" class = "direction"/>
                </div>
                <dt class="son">
                    <a class="url" href="{{__ROOT__}}/admin/goodsList" target="main">
                        商品列表
                    </a>
                </dt>
            </li>
            <li>
                <div class="parent">
                    <img src = "{{__STATIC__}}/images/menu/order_normal.png" class = "icon"/>
                        <span>订单中心</span>
                    <img src = "{{__STATIC__}}/images/menu/direction_right.png" class = "direction"/>
                </div>
                <dt class="son">
                	<a class="url" href="{{__ROOT__}}/admin/orderList" target="main">
                        订单列表
                    </a>
                </dt>
            </li>
            <li>
                <div class="parent">
                    <img src = "{{__STATIC__}}/images/menu/stat_normal.png" class = "icon"/>
                        <span>统计中心</span>
                    <img src = "{{__STATIC__}}/images/menu/direction_right.png" class = "direction"/>
                </div>
                <dt class="son">
                	<a class="url" href="{{__ROOT__}}/admin/mailList" target="main">
                        
                        邮寄订单列表
                    </a>
                </dt>
            </li>
        </ul>
    </div>
    <div id = "container">
            <iframe width=100% height=800px overflow-y:hidden frameborder=0 scrolling=auto name="main" src="{{__ROOT__}}/admin/welcome">
    </div>
</body>
</html>