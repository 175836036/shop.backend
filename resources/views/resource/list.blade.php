@include('public.header')
<title>资源列表</title>
</head>
<body>
    <div id="container">
    	<div id="header">
            <h2 class="title">资源列表</h2>
            <div class="group_button">
                <a href="{{__ROOT__}}/admin/createResource" class="add">添加资源</a>
            </div>
        </div>
        <div id="content">
        	<div id = "handle">
            	<div class="left_group">
                	<form>
                        <select class="select" name = "search_field">
                            <option value = ''>请选择搜索条件</option>
                            <option value = "name" selected="selected">资源名称</option>
                            <option value = "resource_id">资源id</option>
                        </select>
                        <input type="text" class="search_text text"/>
                        <select class="select type_select" name="resource_type">
                            <option value = "">请选择资源类型</option>
                            @foreach($types as $k=>$v)
                                <option value="{{$k}}">{{$v}}</option>
                            @endforeach
                        </select>
                        <button type="button" id="search">搜索</button>
                    </form>
                </div>
                <div class="right_group">
                	
                </div>
            </div>
            <div class="list">
                <table>
                    <thead>
                    	<tr>
                            <th>资源ID</th>
                            <th>资源名称</th>
                            <th>资源类型</th>
                            <th>作者(老师)</th>
                            <th>有效期</th>
                            <th>创建时间</th>
                            <th>审核状态</th>
                            <th class="handle">操作</th>
                        <tr>
                    </thead>
                    <tbody>
                    	
                    </tbody>
                    <tfoot>
                    	<tr>
                        	<td colspan="8">
                            	<div class="page">
                                    <div class="page_info">共1条记录,每页1条,共1页</div>
                                    <div class="page_handle">
                                        跳转至第 <input type="text" value="1" class="page"/> 页,页数<span class="num">1/1</span>
                                        <button class="prev"> < </button>
                                        <button class="next"> > </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</body>
<script>
	var URL = {
		detail : "{{__ROOT__}}/admin/resourceDetail/",
		delete_url : "{{__ROOT__}}/admin/resourceDelete",
		edit_url : "{{__ROOT__}}/admin/resourceEdit/",
		check_url : "{{__ROOT__}}/admin/resourceCheck",
        list_url:"{{__ROOT__}}/admin/resourceList",
        cancle_url:"{{__ROOT__}}/admin/resourceRepeal"
	};
	resource_list($,window);
</script>
</html>